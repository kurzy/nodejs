const https = require('https');

const url = 'https://jsdev.cz/daychecker/?day={day}';

const getRequest = (day) => {
  const reqUrl = url.replace('{day}', day);

  const request = https.get(reqUrl, (response) => {
    const { statusCode, statusMessage } = response;

    if (statusCode !== 200) {
      console.error({ statusCode, statusMessage, reqUrl });
      return;
    }

    const rawData = [];
    response.on('data', (chunk) => rawData.push(chunk));
    response.on('end', () => {
      try {
        const parsedData = JSON.parse(rawData.join(''));
        console.log(parsedData);
      } catch (error) {
        console.error(error);
      }
    });
  });

  request.on('error', (error) => console.error(error));
};

getRequest(6);
