const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((request, response) => {
  switch (request.url) {
    case '/json':
      const man = {
        name: 'Jane',
        surname: 'Bond',
      };

      response.statusCode = 200;
      response.setHeader('Content-Type', 'application/json');
      response.end(JSON.stringify(man));
      break;
    case '/':
      response.statusCode = 200;
      response.setHeader('Content-Type', 'text/plain');
      response.end('This is simple server');
      break;
    default:
      response.statusCode = 404;
      response.setHeader('Content-Type', 'text/plain');
      response.end('404 - not found');
  }

});

server.listen(port, hostname, () => {
  console.log(`Server is running at http://${hostname}:${port}.`);
});
