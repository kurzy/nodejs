setImmediate(() => console.log('immediate done'));

console.log(__filename);
console.log(__dirname);

process.on('beforeExit', (code) => {
  console.log(`Process beforeexit event with code ${code}`);
});

process.on('exit', (code) => {
  console.log(`Process exit event with code ${code}`);
});

// process.exit(1);

console.log(process.env);

const fetchData = async () => {
  const options = {
    method: 'GET',
  };

  const url = new URL('https://jsdev.cz/daychecker/?day=6');

  const data = await fetch(url, options);

  if (data.ok) {
    const result = await data.json();
    console.log(result);
    process.exit();
  }

  console.log('bad day');
  const result = await data.json();
  console.log(result);

  process.exitCode = 1;
  process.exit();
};

fetchData();
