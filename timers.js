setTimeout(() => {
  console.log('Timeout done');
}, 1000);

let index = 0;

const interval = setInterval(() => {
  console.log(index);
  index++;

  if (index > 20) {
    clearInterval(interval);
  }
}, 500);
