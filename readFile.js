const fs = require('fs');
const fsp = require('fs/promises');

const options = {
  encoding: 'utf8',
};

const filename = 'async.txt';

const data = fs.readFileSync(filename, options);
console.log(data);

fs.readFile(filename, (error, data) => {
  if (error) {
    console.error(error);
    process.exit(1);
  }

  console.log(data.toString());

  // for (const value of data.values()) {
  //   console.log(value.toString(2));
  // }
});

const promise = fsp.readFile(filename, options);

promise
  .then((data) => console.log('promise', data))
  .catch((error) => console.error(error));
