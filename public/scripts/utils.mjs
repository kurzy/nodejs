export const log = (text) => {
  const now = new Date();
  const localizedNow = now.toLocaleString();
  return `${localizedNow}:\t\t${text}\n`;
};
