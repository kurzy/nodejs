import { log } from './utils.mjs';
import connectSocket from './websocket.mjs';

const output = document.querySelector('#output');

setTimeout(() => {
  output.textContent = log('Welcome in my magic Node.js and Expressjs page!');
  connectSocket(output, 'ws://127.0.0.1:9200');
}, 1000);
