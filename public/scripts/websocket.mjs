import { log } from "./utils.mjs";

const connectSocket = (element, url) => {
  if (!url || !element) {
    console.error('missing parameters for socket connect function!');
    return;
  }
  const scroll = () => element.scrollTop = element.scrollHeight

  const openHandler = () => {
    element.textContent += log('[open] Connection established');
    element.textContent += log('Sending to server');
    socket.send("Hello from the other side!");
    scroll();
  };

  const messageHandler = (event) => {
    element.textContent += log(`[message] Data received from server: ${event.data}`);
    scroll();
  };

  const closeHandler = (event) => {
    if (event.wasClean) {
      element.textContent += log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
      scroll();
    } else {
      // e.g. server process killed or network down
      // event.code is usually 1006 in this case
      element.textContent += log('[close] Connection died');
      scroll();
    }
  };

  const errorHandler = (error) => {
    console.error(error);
    element.textContent += log(`[error] ${error}`);
    scroll();
  };

  const socket = new WebSocket(url);

  socket.addEventListener('open', openHandler);
  socket.addEventListener('message', messageHandler);
  socket.addEventListener('close', closeHandler);
  socket.addEventListener('error', errorHandler);
};

export default connectSocket;
