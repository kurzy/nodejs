const fs = require('fs');

const lines = [
  'line 1',
  'line 2',
  'line 3',
  'line 4',
  'line 5',
  'line 6',
  'line 7',
  'line 8',
  'line 9',
  'line 10',
];

console.log('Before sync writeFile');

const options = {
  flag: 'w',
};

fs.writeFileSync('sync.txt', lines.join('\n'), options);
fs.writeFileSync('sync.txt', `\n${lines.join('\n')}`, {flag: 'a'});

console.log('After sync writeFile');
