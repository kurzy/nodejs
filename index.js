const crypto = require('crypto');

const TEXT = 'The answer is 42!';

const hash = crypto
  .createHash('MD5')
  .update(TEXT)
  .digest('hex');

  console.log(hash);
