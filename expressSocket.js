const express = require('express');
const http = require('http');
const path = require('path');
const ws = require('ws');

const server = express();

const router = express.Router();
const port = 3000;

server.use(express.static('public'));
server.use('/', router);

router.get('/', (...props) => {
  const [, res] = props;
  res.sendFile(path.resolve(__dirname, 'pages', 'index.html'));
});

router.get('/json', (...props) => {
  const [, res] = props;
  const man = {
    name: 'Adam',
    surname: 'Bernau',
    friends: [
      'Drchlík', 'Ali', 'Emilia', 'Karas',
    ],
    episodes: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
  };

  res.json(man);
});

router.use((...props) => {
  const [, res] = props;
  res.status(404).json({ message: 'Oh no, I can\'t find it!' });
});

const wss = new ws.Server({ port: 9200 });
wss.on('connection', (ws, request) => {
  const ip = request.socket.remoteAddress;
  ws.send(`Welcome, ${ip}, to my WebSocket server!`);

  ws.on('message', (data) => {
    console.log(`WSS received: ${data}`,);
    ws.send(`I recieved this data: ${data}`);
  });

  const start = new Date();
  const timer = setInterval(() => {
    const now = new Date();
    if (now - start > 20000) {
      ws.send('Time is over, closing…');
      ws.close(1000, 'By by, dear…'); //https://developer.mozilla.org/en-US/docs/Web/API/WebSocket/close#code
      // debugger;
      clearTimeout(timer);
      return;
    }

    ws.send(now.toLocaleString('cs'));
  }, 1000)
});

server.listen(port, () => {
  console.log(`Server listening on http://127.0.0.1:${port}`);
});
