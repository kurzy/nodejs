const EventEmitter = require('node:events');

class MyEmitter extends EventEmitter { }

const myEmmiter = new MyEmitter();

const logujB = () => console.log('b');

myEmmiter.on('baf', () => console.log('a'));
myEmmiter.on('baf', logujB);
myEmmiter.once('baf', () => console.log('once'));

myEmmiter.emit('baf');
myEmmiter.emit('baf');

myEmmiter.prependListener('baf', () => console.log('Baf na začátku'));
myEmmiter.prependOnceListener('baf', () => console.log('Baf na začátku once'));

console.log(myEmmiter.listenerCount('baf'));

myEmmiter.emit('baf');
myEmmiter.emit('baf');

console.log(myEmmiter.listenerCount('baf'));

myEmmiter.off('baf', logujB);

myEmmiter.emit('baf');
