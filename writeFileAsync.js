const fs = require('fs');

const lines = [
  'line 1',
  'line 2',
  'line 3',
  'line 4',
  'line 5',
  'line 6',
  'line 7',
  'line 8',
  'line 9',
  'line 10',
];

console.log('Before async writeFile');

const options = {
  flag: 'w',
};

fs.writeFile('async.txt', lines.join('\n'), options, (error) => {
  if (error) {
    console.error('Filewrite not successfull', error);
    process.exit(1);
  }

  console.log('Filewrite successfull');
});

console.log('After async writeFile');
